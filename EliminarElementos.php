<?php

/** 
 * Esta funcion se encargara de eliminar todos los elementos: null, false, 0
 * de un arreglo dado
 */

function eliminar($arr){
    $elementosAEliminar =[false, 0];
    $arrayResultante =[];

    for ($i=0; $i < count($arr) ; $i++) { 
        if (in_array($arr[$i], $elementosAEliminar, true) == false){
            array_push($arrayResultante, $arr[$i] );
        }
    }
    return $arrayResultante;
}

$arr = [0, "Miguel", 38, "jalisco", false,  "Guadalajada",  false, "Ciudad Juarez", false];
print_r(eliminar($arr));

?>