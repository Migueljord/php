<?php
/** 
 * Este codigo cuenta el cuantas veces aparece cada palabra en un texto dado, tomando en cuenta
 * que el texto solo puede recorrerse una sola vez.
 */
function estandarizar($palabra){
    $search = [' ', ',', '.', ';', '...', '?', '!', ':', '-', '_', '"'];
    $resultado = str_replace($search,"",$palabra);
    return strtolower($resultado);
}

function wordRepetitions($text){
    $diccionario = [];
    $textoSeparado = explode(" ",$text);
    foreach ($textoSeparado  as $palabra) {
        $word = estandarizar($palabra);
        if( array_key_exists($word, $diccionario)){
            $diccionario[$word]++;
        }
        else{
            $diccionario[$word]= 1;
        }
    }
    return $diccionario;
}

$texto = 'Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo "Contenido aquí, contenido aquí". demasiado tiempo que un lector se distraerá con el contenido del texto';
$resultado = wordRepetitions($texto);
print_r($resultado);

?>