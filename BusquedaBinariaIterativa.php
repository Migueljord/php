<?php

function binarySearchIterativa($array, $left, $right, $encontrar){
    while ($left <=  $right ){
        $mid = round($left + ($right - $left)/2);
        if ($array[$mid] == $encontrar){
            return $mid;
        }
        if ($array[$mid] < $encontrar){
            $left = $mid + 1;
        }else{
            $right = $mid -1;
        }
    }
    return -1;
}

$array = [1, 5, 10, 43, 101, 202];
$encontrar = 101;
$tamanioArray = count($array);
$resutlado = binarySearchIterativa($array, 0, $tamanioArray-1, $encontrar);
if ($resutlado == -1){
    echo "Elemento no se ha encontrado";
}else{
    echo "El elemento se ha encontrado en la posicion: $resutlado";
}
?>