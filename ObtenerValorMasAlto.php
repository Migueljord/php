<?php
/** 
 * Obtener el numero mas alto de un arreglo pero iterando el arreglo solo unan vez.
 */
function biggest($arr){
    $mayor = $arr[0];
    for ($i=0; $i < count($arr); $i++) { 
        if ($arr[$i] > $mayor){
            $mayor = $arr[$i]; 
        }
    } 
    return $mayor;
}

$arr = [80000, 100, 201, 398, 600, 10100];
echo biggest($arr);
?>