<?php

function binarySearchRecursive($array, $left, $right, $encontrar){
    if ($right >= $left){
        $mid = round($left + ($right - $left)/2);
        if ($array[$mid] == $encontrar){
            return $mid;
        }
        if ($array[$mid] > $encontrar){
            return binarySearchRecursive($array, $left, $mid - 1, $encontrar);
        }
        return binarySearchRecursive($array, $mid + 1, $right, $encontrar);

    }

}

$array = [1, 5, 10, 43, 101, 202];
$encontrar = 101;
$tamanioArray = count($array);
$resutlado = binarySearchRecursive($array, 0, $tamanioArray-1, $encontrar);
if ($resutlado == -1){
    echo "Elemento no se ha encontrado";
}else{
    echo "El elemento se ha encontrado en la posicion: $resutlado";
}
?>